package com.hw.db.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;

import javax.websocket.server.PathParam;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class threadControllerTests {
  private static final String SLUG_STUB = "slug";

  private Thread threadStub;
  private List<Post> postsStub = new ArrayList<>();

  @BeforeEach
  @DisplayName("thread creation")
  void createThreadTest() {
    threadStub = new Thread(
      "Thread",
      new Timestamp(1234567890),
      "bla",
      "bla",
      SLUG_STUB,
      "bla",
      1
    );
  }

  @Test
  @DisplayName("CheckIdOrSlug (id and slug, found and not)")
  void CheckIdOrSlugTest() {
    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
      threadController controller = new threadController();

      /**
       * ID
       */
      threadMock
        .when(() -> ThreadDAO.getThreadById(Integer.parseInt("1")))
        .thenReturn(threadStub);

      assertEquals(
        threadStub,
        controller.CheckIdOrSlug("1"),
        "Thread is not found (id)"
      );

      assertNull(
        controller.CheckIdOrSlug("9999"),
        "Non-existent thread is found (id)"
      );

      /**
       * Slug
       */
      threadMock
        .when(() -> ThreadDAO.getThreadBySlug(SLUG_STUB))
        .thenReturn(threadStub);

      assertEquals(
        threadStub,
        controller.CheckIdOrSlug(SLUG_STUB),
        "Thread is not found (slug)"
      );

      assertNull(
        controller.CheckIdOrSlug("damn son whered you get this"),
        "Non-existent thread is found (slug)"
      );
    }
  }

  @Test
  @DisplayName("CreatePost")
  void createPostTest() {
    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
      threadController controller = new threadController();

      threadMock
        .when(() -> ThreadDAO.getThreadBySlug(SLUG_STUB))
        .thenReturn(threadStub);

      assertEquals(
        ResponseEntity.status(HttpStatus.CREATED).body(postsStub),
        controller.createPost(SLUG_STUB, postsStub),
        "Post not created"
      );
    }
  }

  @Test
  @DisplayName("GetPosts")
  void getPostsTest() {
    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
      threadController controller = new threadController();

      threadMock
        .when(() -> ThreadDAO.getThreadBySlug(SLUG_STUB))
        .thenReturn(threadStub);

      threadMock
        .when(() -> ThreadDAO.getPosts(threadStub.getId(), 1, 1, null, false))
        .thenReturn(postsStub);

      assertEquals(
        ResponseEntity.status(HttpStatus.OK).body(postsStub),
        controller.Posts(SLUG_STUB, 1, 1, null, false),
        "Posts not retrieved"
      );
    }
  }

  @Test
  @DisplayName("changeThread")
  void changeThreadTest() {
    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
      threadController controller = new threadController();

      final String SLUG_STUB_ANOTHER = "SLUG_STUB_ANOTHER";

      Thread threadToBeChanged = new Thread(
        "Sweetpost",
        new Timestamp(9876543210),
        "blah",
        "blah",
        SLUG_STUB_ANOTHER,
        "blah",
        2
      );

      threadToBeChanged.setId(9999);

      threadMock
        .when(() -> ThreadDAO.getThreadBySlug(SLUG_STUB_ANOTHER))
        .thenReturn(threadToBeChanged);

      threadMock
        .when(() -> ThreadDAO.getThreadById(9999))
        .thenReturn(threadStub);

      assertEquals(
        ResponseEntity.status(HttpStatus.OK).body(threadStub),
        controller.change(SLUG_STUB_ANOTHER, threadStub),
        "Thread not changed"
      );
    }
  }

  @Test
  @DisplayName("info")
  void infoTest() {
    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
      threadController controller = new threadController();

      threadMock
        .when(() -> ThreadDAO.getThreadBySlug(SLUG_STUB))
        .thenReturn(threadStub);

      assertEquals(
        ResponseEntity.status(HttpStatus.OK).body(threadStub),
        controller.info(SLUG_STUB),
        "Info not retrieved"
      );
    }
  }

  @Test
  @DisplayName("createVote")
  void createVoteTest() {
    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
      try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
        threadController controller = new threadController();
        User user = new User("some", "some@email.mu", "name", "nothing");
        Vote vote = new Vote("some", 3);

        threadMock
          .when(() -> ThreadDAO.getThreadBySlug(SLUG_STUB))
          .thenReturn(threadStub);

        userMock
          .when(() -> UserDAO.Info(vote.getNickname()))
          .thenReturn(user);

        assertEquals(
          ResponseEntity.status(HttpStatus.OK).body(threadStub),
          controller.createVote(SLUG_STUB, vote),
          "Vote not created"
        );
      }
    }
  }
}
